angular
  .module 'ngFileButton', []
  .directive 'ngFileButton', [
    ->
      {
        restrict : 'A'
        scope :
          ngFileButton : '&'
          accept : '@'
        link : ( scope, $elem, attrs ) ->

          mimetypes = scope.accept.split ','

          $elem.bind 'change', ( e ) ->
            files    = e.target.files
            formData = new FormData()
            for file in files
              formData.append 'files', file if mimetypes.indexOf( file.type ) isnt -1

            scope.ngFileButton() formData

      }
  ]

