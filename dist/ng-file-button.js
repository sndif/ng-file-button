// Generated by CoffeeScript 2.3.2
angular.module('ngFileButton', []).directive('ngFileButton', [
  function() {
    return {
      restrict: 'A',
      scope: {
        ngFileButton: '&',
        accept: '@'
      },
      link: function(scope,
  $elem,
  attrs) {
        var mimetypes;
        mimetypes = scope.accept.split(',');
        return $elem.bind('change',
  function(e) {
          var file,
  files,
  formData,
  i,
  len;
          files = e.target.files;
          formData = new FormData();
          for (i = 0, len = files.length; i < len; i++) {
            file = files[i];
            if (mimetypes.indexOf(file.type) !== -1) {
              formData.append('files',
  file);
            }
          }
          return scope.ngFileButton()(formData);
        });
      }
    };
  }
]);
